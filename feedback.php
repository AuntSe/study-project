<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Контакты</title>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<script src="js/main.js" defer></script>
	<head>
	<body>
		<div id="container">
			<?php
				require("/header.php");
			?>
			<?php
				require("/menu.php");
			?>
			<div class="content">
				<h2>
					Наши контактные данные:
				</h2>
				<p>
				Новосибирск, ул. Ленина, 10, (383)&nbsp;555-66-77
				</p>
				<div id="map-container">
					<img src="/img/map.jpg" id="map">
				</div>
			</div>
			<?php
				require("/footer.php");
			?>
		</div>
	</body>
</html>