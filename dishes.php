<!DOCTYPE html>
<html>
	<head>
		<?php
			require_once("/connection.php");
		?>
		<meta charset="UTF-8">
		<title>Блюда</title>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<script src="js/main.js"></script>
		<script src="js/jquery.js"></script>
		<script src="js/dishes.js">
			
		</script>
	<head>
	<body>
		<div id="container">
			<?php
				require("/header.php");
			?>
			<?php
				require("/menu.php");
			?>
			<div id="dishes-content">
				
				<form method="post" action="category.php">
					<select name="sortdishes">
						<option>Категория:</option>
						<option value="все блюда">Все блюда</option>
						<option value="первые блюда">Первые блюда</option>
						<option value="вторые блюда">Вторые блюда</option>
						<option value="напитки">Напитки</option>
					</select>
					<button type="submit"> Выбрать категорию</button>
				</form>
			<?php
				$category="все блюда";
				$start=0;
				$dishesperpage=16;
				session_start();
				if($_SESSION[numberpage]) {
					$start=$_SESSION[numberpage]-1;
					$start*=$dishesperpage;
				}
				if($_SESSION[category]){
					$category=$_SESSION[category];
				}
				if($category!="все блюда"){
					$result = mysqli_query( $link, "SELECT dminiphoto, dname, dprice, dactionprice, dcategory FROM dishes WHERE dcategory = '$category'");
					$countrows=mysqli_num_rows($result);
					$result = mysqli_query( $link, "SELECT dminiphoto, dname, dprice, dactionprice, dcategory FROM dishes WHERE dcategory = '$category' LIMIT $start,16");
				
				}
				else {
					$result = mysqli_query($link, "SELECT dminiphoto, dname, dprice, dactionprice, dcategory FROM dishes");
					$countrows=mysqli_num_rows($result);
					$result = mysqli_query($link, "SELECT dminiphoto, dname, dprice, dactionprice, dcategory FROM dishes LIMIT $start,16");
				}
				$numpages=ceil($countrows/$dishesperpage);
				while ($mass = mysqli_fetch_assoc($result)) {
					if($mass[dactionprice]!=NULL) {
						echo "<div class='dish'><img src='img/" . $mass[dminiphoto] . "' alt='" 
						. $mass[dname] . "'><p><a href='dish.php' class='dishlink'>". $mass[dname] .
						"</a> <br><s>" . $mass[dprice] . " руб.</s> " .$mass[dactionprice]. " руб.</p></div>";
					}
					else {
						echo "<div class='dish'><img src='img/" . $mass[dminiphoto] . "' alt='" 
						. $mass[dname] . "'><p><a href='dish.php' class='dishlink'>". $mass[dname] .
						"</a> <br>" . $mass[dprice] . " руб.</p></div>";	
					}
				}
				echo "<p id='numpages'> Старницы: ";
				for($i=1;$i<=$numpages;$i++){
					$j=$i-1;
					if($j*$dishesperpage==$start){
						echo "<a href='dishes.php' class='pages' style='border-bottom: none'>" . $i . "</a>&nbsp;";
					}
					else {
						echo "<a href='dishes.php' class='pages'>" . $i . "</a>&nbsp;";
					}		
				}
				echo "</p>";
				$_SESSION[category]="все блюда";
				$_SESSION[numberpage]=1;
			?>
			</div>
			<?php
				require("/footer.php");
			?>
		</div>
		<?php 
			mysqli_free_result($result);
			mysqli_close($link);
		?>
	</body>
</html>