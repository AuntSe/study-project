window.onload=function() 
{
	var b; //Ширина контейнера
	var win; //Ширина окна
	var a = document.getElementsByClassName("dish"); //количество блоков с блюдами всего
	var c;
/*Работа с формой, появляющейся при нажатии кнопки "Заказать обратный звонок"*/

// Форма появляется
document.getElementById("header-btn").onclick = function()
		{
			document.getElementById("header-form").style.display="block";
		}
// Форма исчезает, если поля заполнены (проверка валидности данных производится в html)
document.getElementById("header-form-submit").onclick = function()
		{
			var d;
			d=getElementsByTagName("input").value;
			for(var i=0;i<d.length;i++){
				if(d[i]!=""){
					document.getElementById("header-form").style.display="none";
				}
			}
		}
// Форма исчезает по нажатию клавиши Esc (даже если поля не заполнены - просто данные никуда не отправятся)
window.captureEvents(Event.KEYDOWN); 
window.onkeydown = pressed; 
    function pressed(e) { 
        if(e.which == 27) {
            document.getElementById("header-form").style.display="none";
        }
    }
// При изменении размера окна меняем размер контейнера и положение элементов
	window.addEventListener("resize", resizing);
// Функции, управляющие работой кнопок-стрелок, пролистывающих блоки в галерее блюд
	function arrowRight() {
		var d;
		var c=0;
		for(var i=0; i<a.length;i++) {
			d=document.getElementsByClassName("dish")[i].style.display;
			if(d!="none") {
				c++;
			}
		}
		for(i=0;i<a.length;i++){
			d=document.getElementsByClassName("dish")[i].style.display;
			if(d!="none")
			{
				if(i+c+1>a.length)
				{
				break;
				}
				document.getElementsByClassName("dish")[i].style.display="none";
				document.getElementsByClassName("dish")[i+c].style.display="inline-block";
				document.getElementsByClassName("dish")[i+c].style.marginRight="0";
				document.getElementsByClassName("dish")[i+c-1].style.marginRight="13px";
				break;
			}
		}
	}
	function arrowLeft() {
		var d;
		var c=0;
		for(var i=0; i<a.length;i++) {
			d=document.getElementsByClassName("dish")[i].style.display;
			if(d!="none") {
				c++;
			}
		}
		for(i=a.length-1;i>0;i--){
			d=document.getElementsByClassName("dish")[i].style.display;
			if(d!="none") {
				if(i-c<0)
				{
				break;
				}
				document.getElementsByClassName("dish")[i].style.display="none";
				document.getElementsByClassName("dish")[i-c].style.display="inline-block";
				document.getElementsByClassName("dish")[i-1].style.marginRight="0";
				document.getElementsByClassName("dish")[i-2].style.marginRight="13px";
				break;
			}
		}
	}
/* Работа с расположением элементов на странице в зависимости от ширины контейнера и оазмера окна*/
	function resizing() {
		win = document.documentElement.clientWidth;
		b = document.getElementById("container").offsetWidth;
		if (win<1120) {
			document.getElementById("container").style.width=(win + "px");
			b=win;
			if(win<600) {
				document.getElementById("container").style.width="600px";
				b=600;
			}	
		}
		else {
			document.getElementById("container").style.width=("1120px");
			b=1120;
		}
		// Чтобы не уезжал "подвал"
		if (b<900) {
				document.getElementsByClassName("contacts")[0].style.float="none";
				document.getElementsByClassName("social-icons")[0].style.float="none";
				document.getElementsByClassName("contacts")[0].style.marginLeft="0";
				document.getElementsByClassName("social-icons")[0].style.marginRight="0";
				document.getElementsByClassName("contacts")[0].style.textAlign="center";
				document.getElementsByClassName("social-icons")[0].style.textAlign="center";
				document.getElementsByClassName("contacts")[0].style.marginTop="0";
				document.getElementsByClassName("contacts")[0].style.paddingTop="100px";
				document.getElementsByClassName("social-icons")[0].style.marginTop="10px";
		}
		else {
			document.getElementsByClassName("contacts")[0].style.float="left";
			document.getElementsByClassName("social-icons")[0].style.float="right";
			document.getElementsByClassName("contacts")[0].style.marginLeft="25px";
			document.getElementsByClassName("contacts")[0].style.marginTop="140px";
			document.getElementsByClassName("contacts")[0].style.paddingTop="0";
			document.getElementsByClassName("social-icons")[0].style.marginRight="25px";
			document.getElementsByClassName("social-icons")[0].style.marginTop="127px";
		}
		if (b<1000) {
		// Чтобы не уезжала "шапка"
			document.getElementById("header-text").style.float="none";
			document.getElementById("logo").style.float="none";
			document.getElementById("header-phone").style.float="none";
			document.getElementById("header-btn").style.float="none";
			document.getElementById("header-text").style.marginLeft="0";
			document.getElementById("header-phone").style.marginLeft="0";
			document.getElementById("header-btn").style.marginLeft="auto";
			document.getElementById("logo").style.marginRight="0";
			document.getElementById("header-btn").style.marginRight="auto";
			document.getElementById("header-text").style.textAlign="center";
			document.getElementById("logo").style.textAlign="center";
			document.getElementById("header-phone").style.textAlign="center";
			document.getElementById("header-phone").style.marginTop="5px";
			document.getElementById("header-btn").style.marginTop="15px";
			document.getElementById("header-text").style.fontSize="150%";
			document.getElementById("header-phone").style.fontSize="300%";
		// Чтобы не уезжал блок с доставкой и оплатой
			document.getElementById("delivery-pay").style.float="none";
			document.getElementById("delivery-deliver").style.float="none";
			document.getElementById("delivery-pay").style.marginRight="auto";
			document.getElementById("delivery-pay").style.marginLeft="auto";
			document.getElementById("delivery-deliver").style.marginRight="auto";
			document.getElementById("delivery-deliver").style.marginLeft="auto";
		}
		else {
			// Возвращаем все, как было
			document.getElementById("header-text").style.float="left";
			document.getElementById("logo").style.float="right";
			document.getElementById("header-phone").style.float="left";
			document.getElementById("header-btn").style.float="right";
			document.getElementById("header-text").style.marginLeft="30px";
			document.getElementById("header-text").style.marginTop="15px";
			document.getElementById("header-text").style.fontSize="275%";
			document.getElementById("logo").style.marginRight="30px";
			document.getElementById("logo").style.marginTop="15px";
			document.getElementById("header-phone").style.fontSize="344%";
			document.getElementById("header-phone").style.marginLeft="30px";
			document.getElementById("header-phone").style.marginTop="150px";
			document.getElementById("header-btn").style.marginRight="60px";
			document.getElementById("header-btn").style.marginTop="130px";
			document.getElementById("delivery-pay").style.float="left";
			document.getElementById("delivery-deliver").style.float="right";
		}
		
		/* Работа с галереей блюд*/

		// Выясняем, сколько блоков блюд умещается в контейнер по ширине
		c = parseInt(b/270);
		// Все, что не умещается, скрываем, последнему уместившемуся делаем нулевой марджин
		for (var i=0; i<a.length+1; i++)
			{
			if(i>c)
				{
					document.getElementsByClassName("dish")[i-1].style.display="none";
					document.getElementsByClassName("dish")[i-2].style.marginRight="0";
			}
			else {
				for (var j=0;j<i;j++) {
					document.getElementsByClassName("dish")[j].style.display="inline-block";
					document.getElementsByClassName("dish")[j].style.marginRight="13px";
				}
			}
		}
	}
// В зависимости от изначального размера окна выстраиваем элементы	
	resizing();

	
// Задаем обработчики событий для кнопок-стрелок
	document.getElementsByClassName("arrow")[0].addEventListener("click", arrowLeft);
	document.getElementsByClassName("arrow")[1].addEventListener("click", arrowRight);
	
}	