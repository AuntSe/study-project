<!DOCTYPE html>
<html>
	<head>
		<?php
			session_start();
			$dish=$_SESSION[dish];
			require_once("/connection.php");
			$result = mysqli_query( $link, "SELECT * FROM `dishes` WHERE dname = '$dish'");
			$mass=mysqli_fetch_assoc($result);
		?>
		<meta charset="UTF-8">
		<title><?php echo $mass["dname"]; ?></title>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<script src="js/main.js" defer></script>

	<head>
	<body>
		<div id="container">
			<?php
				require("/header.php");
			?>
			<?php
			require("/menu.php");
			?>
			<div id="about-dish">
				<img src="img/<?php echo $mass["dphoto"]; ?>" alt="<?php echo $mass["dname"]; ?>">
				<h1><?php echo $mass["dname"]; ?></h1>
				<p id="price">
					<?php 
						if($mass[dactionprice]!=NULL) {
							echo "<s>" . $mass["dprice"] . "руб.</s> " . $mass["dactionprice"];
						}
						else {
							echo $mass["dprice"]; 
						}
					?> 
					руб.
				</p>
				<p>
					<?php echo $mass["dabout"]; ?>
				</p>
				<p>
					Состав: <?php echo $mass["dconsist"]; ?>. 
					<?php echo $mass["dsize"]; ?>
				</p>
			</div>
			<?php
				require("/footer.php");
			?>
		</div>	
		<?php 
			mysqli_free_result($result);
			mysqli_close($link);
		?>
	</body>
</html>