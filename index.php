<!DOCTYPE html>
<html>
	<head>
		<?php
			require_once("/connection.php");
		?>
		<meta charset="UTF-8">
		<title>Dobrou chut'! - доставка блюд чешской кухни</title>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		<script src="js/main.js" async></script>
				<script src="js/jquery.js"></script>
		<script>
			$(document).ready(function() {
				$('.dishlink').click( function(e) {
					var number=$('.dishlink').index(e.target)
					var dishname=$(".dishlink:eq('" +number+ "')").html();
					$.post("/scriptic.php", { thisdish: dishname },
					function(data){
						}
					);
				});
				
			});
		</script>
	<head>
	<body>
		<div id="container">
		<?php
				require_once("/connection.php");
			?>
			<?php
				require("/header.php");
			?>
			<?php
				require("/menu.php");
			?>
				<div id="about"> 
					<p>«Dobrou chut’!» - говорят в Чехии перед тем, как приступить к принятию пищи. Чехи - большие любители вкусной и сытной еды, 
				а мы - большие любители Чехии. Мы хотим, чтобы каждый житель Новосибирска мог ощутить себя немного чехом, и поэтому с 
				удовольствием  доставляем вам на дом или в офис свежие блюда чешской кухни, приготовленные по оригинальным рецептам. 
				Настоящая чешская ресторация у вас дома! </p>
				<p>
				Сделайте заказ, позвонив по телефону (383) 777-66-55
					или остваив нам свои контактные данные, чтобы наш оператор 
					мог вам перезвонить.
				</p>
					<p><a href="delivery.php#delivery" target="_blank"> Работаем с 8:00 до 23:00</a></p>
				</div>
			<div id="food-block">
				<?php
					$result = mysqli_query($link, "SELECT dname, dminiphoto, dprice, dactionprice, dcategory, dpromoblock FROM `dishes` WHERE dactionprice IS NOT NULL OR dpromoblock=1");	
				?>
				<p>Популярные блюда, новинки, специальные предложения</p>
				<img src="img/arrow-left.png" class="arrow arrow-left">
				<?php
				while ($mass = mysqli_fetch_assoc($result)) {
					if($mass[dactionprice]!=NULL) {
						echo "<div class='dish'><img src='img/" . $mass[dminiphoto] . "' alt='" 
						. $mass[dname] . "'><p><a href='dish.php' class='dishlink'>". $mass[dname] .
						"</a> <br><s>" . $mass[dprice] . " руб.</s> " .$mass[dactionprice]. " руб.</p></div>";
					}
					else {
						echo "<div class='dish'><img src='img/" . $mass[dminiphoto] . "' alt='" 
						. $mass[dname] . "'><p><a href='dish.php' class='dishlink'>". $mass[dname] .
						"</a> <br>" . $mass[dprice] . " руб.</p></div>";	
					}
				}
				?>
				<img src="img/arrow-right.png" class="arrow arrow-right">
			</div>
			<div id="delivery">
				<div id="delivery-pay">
					<div class="left-side">
						<img src="img/cash.png">
						<img src="img/card.png">
					</div>
					<div class="left-side">
						<a href="delivery.php#pay">Оплата наличными</a>
						<a href="delivery.php#pay">Оплата картой</a>
					</div>
				</div>
				<div id="delivery-deliver">
					<div class="right-side">
						<a href="delivery.php#delivery">Доставка по<br>Новосибирску</a>
						<a href="delivery.php#delivery">Самовывоз</a>
					</div>
					<div class="right-side">
						<img src="img/delivery-delivery.png">
						<img src="img/delivery-self.png">
					</div>
				</div>
			</div>
			<?php
				require("/footer.php");
			?>
		</div>	
		<?php 
			mysqli_free_result($result);
			mysqli_close($link);
		?>
	</body>
</html>